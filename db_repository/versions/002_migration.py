from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
city = Table('city', post_meta,
    Column('id', String(length=300), primary_key=True, nullable=False),
    Column('name', String(length=300)),
    Column('city_type', String(length=150)),
    Column('postcode', String(length=100)),
    Column('rajaongkir_id', Integer),
    Column('date_created', DateTime, nullable=False),
    Column('date_updated', DateTime, nullable=False),
    Column('province_id', String(length=300)),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['city'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['city'].drop()
