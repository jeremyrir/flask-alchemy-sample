from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
city = Table('city', pre_meta,
    Column('id', VARCHAR(length=300), primary_key=True, nullable=False),
    Column('name', VARCHAR(length=300)),
    Column('city_type', VARCHAR(length=150)),
    Column('postcode', VARCHAR(length=100)),
    Column('rajaongkir_id', INTEGER),
    Column('date_created', DATETIME, nullable=False),
    Column('date_updated', DATETIME, nullable=False),
    Column('province_id', VARCHAR(length=300)),
)

city = Table('city', post_meta,
    Column('id', String(length=300), primary_key=True, nullable=False),
    Column('name', String(length=300)),
    Column('city_type', String(length=150)),
    Column('postcode', String(length=100)),
    Column('rajaongkir_city_id', Integer),
    Column('rajaongkir_province_id', Integer),
    Column('date_created', DateTime, nullable=False),
    Column('date_updated', DateTime, nullable=False),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['city'].columns['province_id'].drop()
    pre_meta.tables['city'].columns['rajaongkir_id'].drop()
    post_meta.tables['city'].columns['rajaongkir_city_id'].create()
    post_meta.tables['city'].columns['rajaongkir_province_id'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['city'].columns['province_id'].create()
    pre_meta.tables['city'].columns['rajaongkir_id'].create()
    post_meta.tables['city'].columns['rajaongkir_city_id'].drop()
    post_meta.tables['city'].columns['rajaongkir_province_id'].drop()
