from flask import Flask, render_template, Response, jsonify, abort, request, g, url_for
from flask_httpauth import HTTPBasicAuth
from app import app, db, models, ma
from schema import CustomerSchema, ProvinceSchema
from .forms import LoginForm
from random import choice
from string import ascii_uppercase
import datetime
import requests
import json
import uuid

auth = HTTPBasicAuth()

@app.route('/')
@app.route('/index')
def index():
    user = {'nickname': 'Rama'}  # fake user
    posts = [
        { 
            'author': {'nickname': 'Rama'}, 
            'body': 'Have an awesomeday' 
        },
        { 
            'author': {'nickname': 'Susan'}, 
            'body': 'The Avengers movie was so cool!' 
        }
    ]
    return render_template("index.html",
                           title='Home',
                           user=user,
                           posts=posts)

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        flash('Login requested for OpenID="%s", remember_me=%s' %
              (form.openid.data, str(form.remember_me.data)))
        return redirect('/index')
    return render_template('login.html', 
                           title='Sign In',
                           form=form,
                           providers=app.config['OPENID_PROVIDERS'])

@app.route('/country/create', methods=['GET'])
def save_country():
  c = models.Country(id=str(uuid.uuid1()), name='Indonesia', date_created=datetime.datetime.utcnow(), date_updated=datetime.datetime.utcnow())
  db.session.add(c)
  db.session.commit()

  resp = Response(response="{message: 'OK'}", status=200, mimetype="application/json")
  return(resp)

@app.route('/province/create', methods=['GET'])
def save_province():
  endpoint = "http://pro.rajaongkir.com/api/province"
  headers = {
    'key': "d6fa504399494aec022554a2ef8c71b7",
    'cache-control': "no-cache",
  }

  response = requests.request("GET", endpoint, headers=headers)
  json_data = json.loads(response.text)

  response_status = json_data['rajaongkir']['status']['code'] #Get Status Code
  province_data = json_data['rajaongkir']['results'] #Get array of province

  c = models.Country.query.get('5c45f407-ab55-11e6-ad99-f45c89b13c09')
  for province in province_data:
    p = models.Province(id=str(uuid.uuid1()), name=province['province'], rajaongkir_id=province['province_id'], date_created=datetime.datetime.utcnow(), date_updated=datetime.datetime.utcnow(), country=c)
    db.session.add(p)
    db.session.commit()
    print '''Successfully adding :''', province['province_id'], province['province'] #Print each province and its id
  
  #print(json_data['rajaongkir']['results'])
  resp = Response(response="{message: 'OK'}", status=200, mimetype="application/json")
  return(resp)

@app.route('/city/create', methods=['GET'])
def save_city():
  endpoint = "http://pro.rajaongkir.com/api/city"
  headers = {
    'key': "d6fa504399494aec022554a2ef8c71b7",
    'cache-control': "no-cache",
  }

  # Get Kabupaten based on rajaongkir Province ID
  response = requests.request("GET", endpoint, headers=headers)
  json_data = json.loads(response.text)
  print json_data

  response_status = json_data['rajaongkir']['status']['code'] #Get Status Code
  cities_data = json_data['rajaongkir']['results'] #Get array of province
  for city in cities_data:
    c = models.City(id=str(uuid.uuid1()), name=city['city_name'], rajaongkir_city_id=city['city_id'], rajaongkir_province_id=city['province_id'], city_type=city['type'], postcode=city['postal_code'],date_created=datetime.datetime.utcnow(), date_updated=datetime.datetime.utcnow())
    db.session.add(c)
    db.session.commit()
    print '''Successfully adding :''', city['city_id'], city['city_name']

  #provinces = models.Province.query.all()
  #for province in provinces:
  #  print '''Province''', province.rajaongkir_id, ''' - ''', province.name
  #  province_rajaongkir = str(province.rajaongkir_id)
  #  headers = {
  #    'key': "d6fa504399494aec022554a2ef8c71b7",
  #    'province': province_rajaongkir,
  #    'cache-control': "no-cache",
  #  }
    # Get Kabupaten based on rajaongkir Province ID
  #  response = requests.request("GET", endpoint, headers=headers)
  #  json_data = json.loads(response.text)
  #  print json_data

    # response_status = json_data['rajaongkir']['status']['code'] #Get Status Code
    #cities_data = json_data['rajaongkir']['results'] #Get array of province
    #for city in cities_data:
    #  c = models.City(id=str(uuid.uuid1()), name=city['city_name'], rajaongkir_id=city['city_id'], city_type=city['type'], postcode=city['postal_code'],date_created=datetime.datetime.utcnow(), date_updated=datetime.datetime.utcnow(), province=province)
    #  db.session.add(c)
    #  db.session.commit()
    #  print '''Successfully adding :''', city['city_id'], city['city_name'], ' - ', province.name #Print each city and its id

  #print(json_data['rajaongkir']['results'])
  resp = Response(response="{message: 'OK'}", status=200, mimetype="application/json")
  return(resp)

@app.route('/customer/create', methods=['GET'])
def save_customer():
  fullname = ''.join(choice(ascii_uppercase) for i in range(12))
  c = models.Customer(nickname=fullname, email='ikvi@email.com')
  db.session.add(c)
  db.session.commit()

  resp = Response(response="{message: 'OK'}", status=200, mimetype="application/json")
  return(resp)

@app.route('/customer/', methods=['GET'])
def get_customer():
  #customers = models.Customer.query.all()
  c = models.Customer.query.all() # Get Customer with an ID of 1

  for customer in c:
    print customer.email

  payload = "{}"
  resp = Response(response=payload, status=200, mimetype="application/json")
  return(resp)

@app.route('/address/create', methods=['GET'])
def save_address():
  c = models.Customer.query.get(1) #Get Customer with an ID of 1
  a = models.Address(street='Jalan Kemang Raya', timestamp=datetime.datetime.utcnow(), customer=c)
  db.session.add(a)
  db.session.commit()

@app.route('/api/token/')
@auth.login_required
def get_auth_token():
  token = g.user.generate_auth_token()
  return jsonify({'token': token.decode('ascii')})

@auth.verify_password
def verify_password(username_or_token, password):
  #Authenticate using basic username_password
  #user = models.User.query.filter_by(username=username).first()
  #if not user or not user.verify_password(password):
    #return False
  
  #first try to authenticate by token
  user = models.User.verify_auth_token(username_or_token)
  if not user:
    # if fail, try to authenticate by username
    user = models.User.query.filter_by(username=username_or_token).first()
    if not user or not user.verify_password(password):
      return False

  g.user = user
  return True

@app.route('/api/resource/')
@auth.login_required
def get_resource():
  return jsonify({'data': '(Using Token) My name is %s' % g.user.username})

@app.route('/api/users/<int:id>')
def get_user(id):
  user = models.User.query.get(id)

  if not user:
    abort(400)
  return jsonify({'username': user.username})

@app.route('/api/users/', methods=['POST'])
def new_user():
  username = request.json.get('username')
  password = request.json.get('password')

  if username is None or password is None:
    abort(400)
  if models.User.query.filter_by(username = username).first() is not None:
    abort(400)
  user = models.User(username = username)
  user.hash_password(password)
  db.session.add(user)
  db.session.commit()
  return jsonify({'username':user.username}), 201, {'location': url_for('get_user', id=user.id, _external = True)}