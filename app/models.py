from passlib.apps import custom_app_context as pwd_context
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)
from app import app,db, ma

def dump_datetime(value):
    """Deserialize datetime object into string form for JSON processing."""
    if value is None:
        return None
    return [value.strftime("%Y-%m-%d"), value.strftime("%H:%M:%S")]

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(100), index=True)
    password_hash = db.Column(db.String(128))

    def generate_auth_token(self, expiration=600):
        s = Serializer(app.config['SECRET_KEY'], expires_in = expiration)
        return s.dumps({'id':self.id})

    def hash_password(self, password):
        self.password_hash = pwd_context.encrypt(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password_hash)

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None #Token is valid, but already expired
        except BadSignature:
            return None #Invalid Token

        user = User.query.get(data['id'])
        return user

class Customer(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nickname = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    desc = db.Column(db.String(200))
    address = db.relationship('Address', backref='customer', lazy='dynamic')

class Address(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    street = db.Column(db.String(140))
    building = db.Column(db.String(140))
    province = db.Column(db.String(140))
    timestamp = db.Column(db.DateTime)
    user_id = db.Column(db.Integer, db.ForeignKey('customer.id'))

    def __repr__(self):
        return '<Address %r>' % (self.street)

class Country(db.Model):
    id = db.Column(db.String(300), primary_key = True, index = True, unique = True)
    name = db.Column(db.String(300), index = True, unique = True)
    date_created = db.Column(db.DateTime, nullable = False)
    date_updated = db.Column(db.DateTime, nullable = False)
    province = db.relationship('Province', backref='country', lazy='dynamic')

    def __repr__(self):
        return '<Country %r>' % (self.name)

class Province(db.Model):
    id = db.Column(db.String(300), primary_key = True, index = True, unique = True)
    name = db.Column(db.String(300))
    rajaongkir_id = db.Column(db.Integer)
    date_created = db.Column(db.DateTime, nullable = False)
    date_updated = db.Column(db.DateTime, nullable = False)
    country_id = db.Column(db.String(300), db.ForeignKey('country.id'))

    def __repr__(self):
        return '<Province %r>' % (self.name)

class City(db.Model):
    id = db.Column(db.String(300), primary_key = True, index = True, unique = True)
    name = db.Column(db.String(300))
    city_type = db.Column(db.String(150))
    postcode = db.Column(db.String(100))
    rajaongkir_city_id = db.Column(db.Integer)
    rajaongkir_province_id = db.Column(db.Integer)
    date_created = db.Column(db.DateTime, nullable = False)
    date_updated = db.Column(db.DateTime, nullable = False)

    def __repr__(self):
        return '<City %r>' % (self.name)