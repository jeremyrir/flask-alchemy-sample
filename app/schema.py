from app import ma
from models import Customer, Address, Province, Country, City

class CustomerSchema(ma.ModelSchema):
    class Meta:
        model = Customer

class AddressSchema(ma.ModelSchema):
    class Meta:
        model = Address

class ProvinceSchema(ma.ModelSchema):
    class Meta:
        model = Province

class CitySchema(ma.ModelSchema):
    class Meta:
        model = City